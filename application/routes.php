<?php 
    $routes['records'] = 'record/all';
    $routes['insert'] = 'record/insert';
    $routes['delete'] = 'record/delete';
    $routes['update'] = 'record/update';    
    $routes['search'] = 'record/search';    
    $routes['default'] = 'record/index';
?>