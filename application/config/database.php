<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'default' => array
    (
        'type'       => 'mysqli',
        'connection' => array
        (
            'hostname'   => 'localhost',
            'username'   => 'root',
            'password'   => '',
            'database'   => 'kohana',
            'persistent' => FALSE,
        ),
        'table_prefix' => '',
        'charset'      => 'utf8',
        'caching'      => FALSE,
        'profiling'    => TRUE,
    ),
);