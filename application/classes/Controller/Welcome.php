<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {
	public function action_index(){
		$this->model = new Model_Records;
		$view = View::factory('mypage');
		$records = $this->model->getRecords();
		$view->bind('records', $records);

		$this->response->body($view);
	}
	public function action_update(){
		$this->model = new Model_Records;
		$error = array();
		$success = array();


		if(!empty($_FILES["edit_file"]["tmp_name"])){
			$target_dir = APPPATH . 'uploads/';
			$target_file = $target_dir . basename($_FILES["edit_file"]["name"]);
			$uploadOk = 1;
			$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
		
			$check = getimagesize($_FILES["edit_file"]["tmp_name"]);
			if($check !== false) {
				$error[] = "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				$error[] = "File is not an image.";
				$uploadOk = 0;
			}
	
			if(move_uploaded_file($_FILES["edit_file"]["tmp_name"], $target_file)) {
				$success[] = "The file ". basename( $_FILES["edit_file"]["name"]). " has been uploaded.";
			} else {
				$error[] = "Sorry, there was an error uploading your file.";
			}
			
			if(count($success)){
				$data['filename'] = basename( $_FILES["edit_file"]["name"]);
				$data['title'] = $_REQUEST['edit_title'];
				$data['id'] = $_REQUEST['id'];
	
				$this->model->updateRecord($data);
			}
		}else{
			$data['title'] = $_REQUEST['edit_title'];
			$data['id'] = $_REQUEST['id'];

			$data = $this->model->updateRecordNoImage($data);
		}

		

		echo json_encode(array('success'=>$success, 'error'=>$error, 'data'=>$data));
		exit();
	}

	public function action_upload(){
		if(isset($_REQUEST['id']) && !empty($_REQUEST['id'])){
			$this->action_update();
			return;
		}

		$this->model = new Model_Records;
		$error = array();
		$success = array();
		$target_dir = APPPATH . 'uploads/';
		$target_file = $target_dir . basename($_FILES["file"]["name"]);
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
	
		$check = getimagesize($_FILES["file"]["tmp_name"]);
		if($check !== false) {
			$error[] = "File is an image - " . $check["mime"] . ".";
			$uploadOk = 1;
		} else {
			$error[] = "File is not an image.";
			$uploadOk = 0;
		}

		if(move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
			$success[] = "The file ". basename( $_FILES["file"]["name"]). " has been uploaded.";
		} else {
			$error[] = "Sorry, there was an error uploading your file.";
		}
		
		if(count($success)){
			$data['filename'] = basename( $_FILES["file"]["name"]);
			$data['title'] = $_REQUEST['title'];

			$this->model->insertRecord($data);
		}

		echo json_encode(array('success'=>$success, 'error'=>$error, 'data'=>$data));
		exit();
	}

	public function action_edit(){
		$this->model = new Model_Records;
		$record = $this->model->getRecord($this->request->param('id'));
		echo json_encode($record);
		exit();
	}

	public function action_delete(){
		$this->model = new Model_Records;
		$record = $this->model->deleteRecord($this->request->param('id'));
		echo json_encode($record);
		exit();
	}

	public function action_records(){
		$this->model = new Model_Records;
		$records = $this->model->getRecords();
		echo json_encode($records);
		exit();
	}

} // End Welcome
