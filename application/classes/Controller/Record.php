<?php 
    defined('SYSPATH') or die('No direct script access.');

    class Controller_Record extends Controller {
        private function __init(){
            //Initialize all dependencies
            $this->model = new Model_Records; //load Records Model
            $this->view = View::factory('records'); //set records view file
        }

        private function load(){
            $this->response->body($this->view);
        }

        public function action_index(){
            $this->__init();
            $this->load();
        }

        public function action_all(){
            $this->__init();
            echo json_encode(
                array(
                    'data' => $this->model->getRecords()
                )
            );
            exit();
        }

        public function action_insert(){
            $this->__init();
            $file = $_FILES['file'];
            $data = array();

            if(isset($_REQUEST['title']) && !empty($_REQUEST['title'])){
                $data['title'] = addslashes($_REQUEST['title']);            
            }else{
                $data['title'] = 'Untitled';
            }

            if(empty($_FILES['file']['name'])){
                $data['filename'] = 'placeholder.jpg';
            }else{
                $data['filename'] = addslashes($file['name']);
                $target_dir = APPPATH . 'uploads\\';
                if(!move_uploaded_file($file['tmp_name'], $target_dir . $file['name'])){
                    echo json_encode(false);
                    exit();
                }
            }

            echo json_encode($this->model->insertRecord($data));
            exit();
        }

        public function action_delete(){
            $this->__init();
            echo json_encode($this->model->deleteRecord($_REQUEST['id']));
            exit();
        }

        public function action_update(){
            $this->__init();
            $hasFile = false;

            if(!empty($_FILES['file']['name'])){
                $file = $_FILES['file'];
                $data['filename'] = addslashes($file['name']);
                $hasFile = true;

                
                $target_dir = APPPATH . 'uploads\\';
                if(!move_uploaded_file($file['tmp_name'], $target_dir . $file['name'])){
                    echo json_encode(false);
                    exit();
                }
            }

            $file = $_FILES['file'];
            $data['title'] = addslashes($_REQUEST['title']);
            $data['id'] = $_REQUEST['id'];

            if($hasFile){
                $res = $this->model->updateRecord($data);
            }else{
                $res = $this->model->updateRecordNoImage($data);
            }
            
            echo json_encode($res);
            exit();
        }

        public function action_search(){
            $this->__init();
            $string = addslashes($_REQUEST['string']);
            
            echo json_encode(
                array(
                    'data' => $this->model->searchRecord($string) 
                )
            );
            exit();
        }

    }

?>