<?php 
    
    class Model_Records extends Model{
        public function insertRecord($data){
            return DB::query(Database::INSERT, "INSERT INTO tbl_records SET title='{$data['title']}', filename='{$data['filename']}'")->execute();
        }

        public function getRecords(){
            return DB::query(Database::SELECT, "SELECT * FROM tbl_records")->execute()->as_array();  
        }

        public function getRecord($id){
            return DB::query(Database::SELECT, "SELECT * FROM tbl_records WHERE id=$id")->execute()->as_array();            
        }

        public function deleteRecord($id){
            return DB::query(Database::DELETE, "DELETE FROM tbl_records WHERE id=$id")->execute();                        
        }

        public function updateRecord($data){
            return DB::query(Database::UPDATE, "UPDATE tbl_records SET title='{$data['title']}', filename='{$data['filename']}' WHERE id={$data['id']}")->execute();
        }

        public function updateRecordNoImage($data){
            $id = $data['id'];
            $title = $data['title'];
            return DB::query(Database::UPDATE, "UPDATE tbl_records SET title='$title' WHERE id=$id")->execute();   
        }

        public function searchRecord($string){
            return DB::query(Database::SELECT, "SELECT * FROM tbl_records WHERE title LIKE '%$string%' OR filename LIKE '%$string%'")->execute()->as_array();            
        }
    }
?>