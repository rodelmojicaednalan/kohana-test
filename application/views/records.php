<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Records</title>
    <link rel="stylesheet" href="<?= URL::base() ?>assets/dist/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="<?= URL::base() ?>assets/css/records-style.css">        
</head>
<body>
    <nav>
        <ul>
            <li class="active"><a href="<?= URL::base() ?>"> <i class="fa fa-files-o"></i> Records</a></li>
        </ul>
    </nav>
    <br>

    <div class="container">
        <h2><i class="fa fa-files-o"></i> Database Records</h2>
        <hr>
        
        <div class="row">
            <div class="pull-left"> 
                <button id="btn_add" class="btn btn-default"><i class="fa fa-plus"></i> Create new record </button>
            </div>
            <div class="pull-right">
                <div class="search-wrapper">
                    <span class="fa fa-search"></span>
                    &nbsp;<input type="search" name="search" id="search" placeholder="Search filename here">
                </div>
            </div>
        </div>
        
        <div class="row">
            <table class="highlight">
                <thead>
                    <tr>
                        <th>Title</th>
                        <th>Thumbnail</th>
                        <th>Filename</th>
                        <th>Date Added</th>
                    </tr>
                </thead>
                <tbody id="table_container"></tbody>
            </table>
        </div>
    </div>

    <!-- Add Modals -->
    <div class="modal" id="add_modal">
        <div class="modal-content">
            <form action="insert" method="POST">
                <h3><i class="fa fa-plus"></i> Create new record</h3>
                <hr>
                <div class="columns">
                    <div class="item-6">
                        <img class="preview" id="add_preview" src="<?= URL::base() ?>application/uploads/placeholder.jpg" alt="">
                    </div>
                    <div class="item-6">
                        <div class="container">
                            <label for="">Title</label>
                            <input type="text" name="title" class="form-control" placeholder="Enter title">
                            <input type="file" name="file" class="form-control" id="add_upload" data-target="#add_preview">    
                            <button class="btn btn-success btn-block" type="submit"><i class="fa fa-save"></i> Save Record</button>
                            <button type="reset" class="btn close btn-default">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Edit Modals -->
    <div class="modal" id="edit_modal">
        <div class="modal-content">
            <h3><i class="fa fa-plus"></i> Update record</h3>
            <hr>
            <div class="columns">
                <div class="item-6">
                    <img class="preview" id="edit_preview" src="<?= URL::base() ?>application/uploads/placeholder.jpg" alt="">
                </div>
                <div class="item-6">
                    <div class="container">
                        <form action="update" method="POST">
                            <label for="">Title</label>
                            <input type="hidden" name="id">
                            <input type="text" name="title" class="form-control" placeholder="Enter title">
                            <input type="file" name="file" class="form-control" id="edit_upload" data-target="#edit_preview">    
                            <button class="btn btn-success btn-block" type="submit"><i class="fa fa-save"></i> Update Record</button>
                        </form>
                        <form action="delete" method="POST">
                            <input type="hidden" name="id" id="delete_id">
                            <button class="btn btn-danger btn-block" type="submit"><i class="fa fa-trash"></i> Delete Record</button>
                        </form>
                    <button type="reset" class="btn close btn-default">Cancel</button>
                </div>
            </div>
        </div>
        </div>
    </div>

    <div class="center">
        <div class="pagination"></div>
    </div>



    <!-- Overlay -->
    <div class="overlay"></div>
    <div class="loader-overlay">
        <div class="loader"></div>
    </div>

    <footer>
        <div class="footer-content">
            All Rights Reserved &copy; <?= date("Y") ?>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script>
        window.base_url = `<?= URL::base() ?>`; 
    </script>
    <script src="<?= URL::base() ?>assets/js/records.js"></script>
</body>
</html>-