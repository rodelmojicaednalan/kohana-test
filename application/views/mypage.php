<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="<?= URL::base();?>assets/css/custom.css">

</head>
<body>
    <div class="container">
        <button id="myBtn" class="btn btn-primary pull-right margin-bottom-sm">Add Record</button>

        <table>
            <tr>
                <th>Title</th>
                <th>Thumbnail</th>
                <th>Filename</th>
                <th>Date added</th>
                <th>Settings</th>
            </tr>
            <tbody id="table_container"></tbody>
        </table>
        <div id="myModal" class="modal">

            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    <h2>Add Record</h2>
                </div>
                <div class="modal-body">
                    <br><br>
                    <form id="form">
                    <div id="add_mode">
                        <label for="">Title</label><br>
                        <input type="text" name="title" placeholder="Enter title">
                        <br><br>
                        <div class="upload-btn-wrapper">
                            <button class="btn btn-default">Upload a file</button>
                            <span class="filepath"></span>
                            <input type="file" name="file" id="file">
                        </div>
                        <br><br>
                        <button class="btn btn-success pull-right" type="submit">Upload</button>
                    </div>

                    <div id="edit_mode">
                        <input type="hidden" name="id">
                        <label for="">Title</label><br>
                        <input type="text" name="edit_title" placeholder="Enter title">
                        <br><br>
                        <div class="container" style="text-align:center">
                            <img name="edit_img" class="thumbnail" src="">
                        </div>
                        <br><br>
                        <div class="upload-btn-wrapper">
                            <button class="btn btn-default">Upload a file</button>
                            <span class="filepath"></span>
                            <input type="file" name="edit_file">
                        </div>
                        <br><br>
                        <button class="btn btn-success pull-right" type="submit">Save Changes</button>
                        </div>
                        <br><br><br>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <script src="https://code.jquery.com/jquery-1.12.4.min.js" crossorigin="anonymous"></script>
    <script>
        window.base_url = `<?= URL::base() ?>`; 
    </script>
    <script src="<?= URL::base()?>assets/js/custom.js"></script>
</body>
</html>