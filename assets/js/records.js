(function(){
    var elements = {
        buttons: {},
        modals: {},
        containers: {},
        forms: {},
        inputs: {}
    };

    class Modal {
        constructor(selector){
            this.element = document.querySelector(selector + '.modal');
            this.isOpen = false;
            this.bindCloseEvent();
        }

        bindCloseEvent(){
            this.element.querySelector('.close').addEventListener('click',()=>{
                this.hide();
            });
        }

        show(){
            this.element.classList.add('open');
            this.isOpen = true;
            app.overlay.show();
        }

        hide(){
            this.element.classList.remove('open');
            this.isOpen = false;
            app.overlay.hide();
            this.resetForms()
        }

        resetForms(){
            var forms = document.querySelectorAll('form');
            for(var i =0; i < forms.length; i++){
                forms[i].reset();
            }
        }
    }

    class Record {
        constructor(){
            console.info("Started Initialization");
            this.initElements();
            this.initLoaderAndOverlay();
            this.fetchRecords();
            this.bindEvents();
            this.bindFileChange();
            this.bindSubmitEvent();
            this.bindSearch();
            console.info("Initialized.");
        }
        
        initElements(){
            elements.inputs.search = document.getElementById('search');
            elements.buttons.add = document.getElementById('btn_add');
            elements.modals.add =  new Modal('#add_modal');
            elements.modals.edit =  new Modal('#edit_modal');
            elements.containers.table = document.getElementById('table_container');
            elements.containers.pagination = document.querySelector('.pagination');
            elements.forms.add = document.querySelector('form[action=insert]');
            elements.forms.edit = document.querySelector('form[action=update]');
        }

        initLoaderAndOverlay(){
            window.app = {
                overlay: {
                    show: function(){
                        document.querySelector('.overlay').style['display'] = 'block';
                    },
    
                    hide: function(){
                        document.querySelector('.overlay').style['display'] = 'none';
                    }
                },

                loader: {
                    show: function(){
                        document.querySelector('.loader-overlay').style['display'] = 'block';
                    },

                    hide: function(){
                        document.querySelector('.loader-overlay').style['display'] = 'none';                        
                    }
                },

                pagination: 1,
                per_page: 3
            };
        }

        bindEvents(){
            elements.buttons.add.addEventListener('click', function(){
                if(elements.modals.add.isOpen){
                    elements.modals.add.hide();
                }else{
                    elements.modals.add.show();
                }
            });

            $(document).on('click', 'tbody tr', function(){
                var data = JSON.parse(this.dataset.data);
                elements.forms.edit.querySelector('input[name=id]').value = data.id;
                elements.forms.edit.querySelector('input[name=title]').value = data.title;
                var img = elements.forms.edit.querySelector('input[type=file]').dataset.target;
                document.querySelector(img).src = window.base_url + 'application/uploads/'+ data.filename;
                document.getElementById('delete_id').value = data.id;
                elements.modals.edit.show();
            });
        }

        bindFileChange(){
            var input_file = document.querySelectorAll('input[type=file]');
            for(var i=0; i < input_file.length; i++){
                input_file[i].addEventListener('change', function(){
                    if(!this.files.length || !this.dataset.target){
                       return; 
                    }
    
                    var file = this.files[0];
                    var rdr = new FileReader();
                    var target = document.querySelector(this.dataset.target);
                    rdr.onloadend = function(e){
                        target.src = e.target.result;
                    }
                    rdr.readAsDataURL(file);
                });
            }

            $(document).on('reset', 'form', function(){
                if(this.querySelector('img')){
                    this.querySelector('img').src = window.base_url + 'application/uploads/placeholder.jpg';
                }
            });
        }

        fetchRecords(){
            app.loader.show();

            $.ajax({
                url: `${window.base_url}records`,
                type: 'GET',
                success: (res)=>{
                    this.printRecords(res.data);
                    app.loader.hide();
                },
                error: function(err){
                    app.loader.hide();
                    console.log(err);   
                },
                dataType: 'json'
            });
        }

        printRecords(data = false){
            if(data){
                app.data = data;
            }else{
                data = app.data;
            }

            var start = ((app.pagination - 1) * app.per_page);
            var fragments = '';

            for(var i=start; i < start + app.per_page; i++){
                if(data[i]){
                    fragments += `<tr data-data='${JSON.stringify(data[i])}'>
                        <td>${data[i].title}</td>
                        <td>
                            <img class="thumbnail" src="${window.base_url}application/uploads/${data[i].filename}"
                        </td>
                        <td>${data[i].filename}</td>
                        <td>${data[i].date_added}</td>
                    </tr>`;
                }
            }
            elements.containers.table.innerHTML = fragments;
            this.initializePagination();

            if(!data.length){
                this.printNoRecords();
            }
        }

        bindSubmitEvent(){
            var $this = this;
            $(document).on('submit', 'form', function(e){
                e.preventDefault();

                var url =  this.action;
                var type = this.method;
                var data = new FormData(this);

                $.ajax({
                    url: url,
                    type: type,
                    data: data,
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: ()=>{
                        elements.modals.add.hide();
                        elements.modals.edit.hide();
                        $this.fetchRecords();
                    }
                });
            });
        }

        bindSearch(){
            var $this = this;
            elements.inputs.search.addEventListener('keyup', function(e){
            
                var value = this.value;
                $.ajax({
                    url: window.base_url + 'search',
                    type: 'GET',
                    data: {
                        string: value
                    },
                    success: function(res){
                        if(res.data.length){
                            $this.printRecords(res.data);
                        }else{
                            $this.printNoRecords();
                        }
                    },
                    dataType: 'json'
                });
                
            });
        }

        printNoRecords(){
            var message = '<h4 style="color: red; text-align: center; position: absolute; left: 0; right: 0"><i class="fa fa-warning"></i> No record found</h4>';
            elements.containers.table.innerHTML = message;
        }

        initializePagination(){
            var $this = this;
            app.total_page = Math.ceil(app.data.length / app.per_page);
            var pagination = '';
            var isactive = '';

            for(var i=0; i < app.total_page; i++){
                isactive = '';
                if(i+1 == app.pagination){
                    isactive =  'active';    
                }
                pagination += `<a class="${isactive}" href="javascript: void(0)" data-page="${i+1}">${i+1}</a>`;
            }

            elements.containers.pagination.innerHTML = pagination;
            var width = (document.querySelector('body').offsetWidth / 2) -  (elements.containers.pagination.offsetWidth / 2);
            elements.containers.pagination.style['left'] = width + 'px';

            if(app.pagination > app.total_page){
                $('.pagination a:last-child').click();
            }

            $(document).on('click', '.pagination a', function(){
                app.pagination = this.dataset.page;
                $this.printRecords();
            });
        }
    }

    $(document).ready(function(){
        new Record();
    });
})();