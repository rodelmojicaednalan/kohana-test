(function(){
    var modal = document.getElementById('myModal');
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    var table_container = document.getElementById('table_container');
    var edit_upload = document.querySelector('[name=edit_file]');

    btn.onclick = function() {
        modal.style.display = "block";
    }
    span.onclick = function() {
        modal.style.display = "none";
        form.reset();
        
    }
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            form.reset();
        }
    }

    var form = document.getElementById('form');
    form.addEventListener('reset', function(){
        set_mode('add');
        $('.filepath').text('');
    });

    form.addEventListener('submit', function(e){
        e.preventDefault();
        data = new FormData(this);

        $.ajax({
            url: `${window.base_url}file/upload`,
            type: 'POST',
            data: data,
            cache: false,
            processData: false,
            contentType: false,
            success: function(res){
                fetchRecords(function(data){
                    printRecords(data);
                });
                set_mode('add');
                $('.close').click();
            },
            error: function(err){
                console.log(err);                
            },
            dataType: 'json'
        })
    });

    
    $('input[type=file]').change(function(){
        if(this.files.length){
            $('.filepath').text(this.files[0].name);
        }
    });

    edit_upload.addEventListener('change', function(){
        if(!this.files.length){
            return false;
        }

        var file = this.files[0];
        var rdr = new FileReader();
        rdr.onloadend = function(e){
            $('[name=edit_img]').attr('src', e.target.result);
        };

        rdr.readAsDataURL(file);
    });

    $(document).on('click', '.edit', function(){
        var id = this.dataset.id;
        $.ajax({
            url: `${window.base_url}file/edit/${id}`,
            type: 'GET',
            success: function(res){
                set_mode('edit');
                $('[name=edit_title]').val(res[0].title);
                $('[name=id]').val(res[0].id);
                $('[name=edit_filename]').val(res[0].filename);
                $('[name=edit_img]').attr('src', window.base_url + 'application/uploads/' + res[0].filename);
                $('#myBtn').click();
                fetchRecords(function(data){
                    printRecords(data);
                });
            },
            error: function(err){
                console.log(err);                
            },
            dataType: 'json'
        })
    });

    $(document).on('click', '.delete', function(){
        var id = this.dataset.id;
        $.ajax({
            url: `${window.base_url}file/delete/${id}`,
            type: 'GET',
            success: function(res){
                fetchRecords(function(data){
                    printRecords(data);
                });
            },
            error: function(err){
                console.log(err);                
            },
            dataType: 'json'
        })
    });

    function fetchRecords(callback){
        $.ajax({
            url: `${window.base_url}file/records`,
            type: 'GET',
            success: function(res){
                callback(res);
            },
            error: function(err){
                console.log(err);                
            },
            dataType: 'json'
        });
    }

    function set_mode(mode){
        switch(mode){
            case 'edit': {
                $('#edit_mode').show();
                $('#add_mode').hide();
                break;
            }
            case 'add': {
                $('#edit_mode').hide();
                $('#add_mode').show();
                break;
            }
        }
    }

    function printRecords(data){
        var fragments = '';
        for(var i=0; i < data.length; i++){
            fragments += `<tr>
                <td>${data[i].title}</td>
                <td><img class="thumbnail" src="http://localhost/kohana/application/uploads/${data[i].filename}"></td>
                <td>${data[i].filename}</td>
                <td>${data[i].date_added}</td>
                <td>
                    <button class="edit btn btn-success" data-id="${data[i].id}">Edit</button>
                    <button class="delete btn btn-danger" data-id="${data[i].id}">Delete</button>
                </td>
            </tr>`;
        }

        table_container.innerHTML = fragments;
    }   

    fetchRecords(function(data){
        printRecords(data);
        set_mode('add');
    });  
})();